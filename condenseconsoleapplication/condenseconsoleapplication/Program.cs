﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace condenseconsoleapplication
{
    class Program
    {
        static void Main(string[] args)
        {
            UpdateCondenseDB();

            //UpdatePatientOnly();

            //UpdateAllergyOnly();

            //UpdateDiseaseOnly();

            //UpdateMedicationOnly();

            //UpdatePatientNoteOnly();

            //UpdateProcedureNoteOnly();

            //UpdateProcedureLogeOnly();



        }

        static void UpdateProcedureLogeOnly()
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            string strCount = ConfigurationManager.AppSettings["Itterations"];
            long lcount = 0;
            try
            {
                lcount = Convert.ToInt64(strCount);
            }
            catch
            {
                lcount = 0;
            }
            for (int i = 0; i < lcount; i++)
            {

                Console.Write("Processing ProcedureLog Batch" + " -- " + i.ToString() + "\r\n");

                DataTable dataTable = cDental.GetRecords("Select * from [dbo].[vwProcedureLogBatch]", false, false, false, false, false,true, false);

                foreach (DataRow drPatient in dataTable.Rows)
                {
                    try
                    {

                        long lPatNum = Convert.ToInt64(drPatient["PatNum"]);
                        Console.Write("ProcedureLog - " + lPatNum.ToString() + "-- " + DateTime.Now.ToLongTimeString() + "\r\n");
                        if (lPatNum > 0)
                        {

                            UpdateProcedureLog(lPatNum);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        static void UpdateProcedureLog(long lPatNum)
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            DataTable dataTable = cDental.GetProcedureLog(lPatNum);
            if (dataTable.Rows.Count == 0)
            {
                dataTable.Rows.Add("BARTON_Opendent", "No Records", 0, lPatNum);//, 0, "", "1/1/2001 12:00:00 AM", 0.0, "", "", "", 0, 0, 0, 0, 0, 0, "", "1/1/2001 12:00:00 AM", "", "1/1/2001 12:00:00 AM", 0, "", "", 0, 0, 0, 0, 0, "", "", "", "", "", 0, 0, 0, 0, "1/1/2001 12:00:00 AM", 0, 0, "", "1/1/2001 12:00:00 AM", "1/1/0001 12:00:00 AM", "1/1/2001 12:00:00 AM", 0, 0, 0, 0, 0, 0, "", 0, "", "", "", "", 0, 0.0);
            }
            else
            {
                bool bDataRowDiffernet = cDental.IsDataRowEqual(dataTable);
                if (!bDataRowDiffernet)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        dr["status"] = "Differences";
                    }
                }
            }
            cDental.TruncateandInsertData("ProcedureLog", dataTable, lPatNum);
        }

        static void UpdateProcedureNoteOnly()
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            string strCount = ConfigurationManager.AppSettings["Itterations"];
            long lcount = 0;
            try
            {
                lcount = Convert.ToInt64(strCount);
            }
            catch
            {
                lcount = 0;
            }
            for (int i = 0; i < lcount; i++)
            {

                Console.Write("Processing ProcedureNote Batch" + " -- " + i.ToString() + "\r\n");

                DataTable dataTable = cDental.GetRecords("Select * from [dbo].[vwProcedureNoteBatch]", false, false, false, false, false, false, true);

                foreach (DataRow drPatient in dataTable.Rows)
                {
                    try
                    {

                        long lPatNum = Convert.ToInt64(drPatient["PatNum"]);
                        Console.Write("ProcedureNote - " + lPatNum.ToString() + "-- " + DateTime.Now.ToLongTimeString() + "\r\n");
                        if (lPatNum > 0)
                        {

                            UpdateProcedureNote(lPatNum);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        static void UpdateProcedureNote(long lPatNum)
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            DataTable dataTable = cDental.GetProcedureNote(lPatNum);
            if (dataTable.Rows.Count == 0)
            {
                dataTable.Rows.Add("BARTON_Opendent", "No Records", 0, lPatNum);//, "", 0, "1/1/0001 12:00:00 AM", "1/1/0001 12:00:00 AM", "", "", 0, 0, 0, "");
            }
            else
            {
                bool bDataRowDiffernet = cDental.IsDataRowEqual(dataTable);
                if (!bDataRowDiffernet)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        dr["status"] = "Differences";
                    }
                }
            }
            cDental.TruncateandInsertData("ProcNote", dataTable, lPatNum);
        }

        static void UpdatePatientNoteOnly()
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            string strCount = ConfigurationManager.AppSettings["Itterations"];
            long lcount = 0;
            try
            {
                lcount = Convert.ToInt64(strCount);
            }
            catch
            {
                lcount = 0;
            }
            for (int i = 0; i < lcount; i++)
            {

                Console.Write("Processing PatientNote Batch" + " -- " + i.ToString() + "\r\n");

                DataTable dataTable = cDental.GetRecords("Select * from [dbo].[vwPatientNoteBatch]", false, true, false, false, false, false, false);

                foreach (DataRow drPatient in dataTable.Rows)
                {
                    try
                    {

                        long lPatNum = Convert.ToInt64(drPatient["PatNum"]);
                        Console.Write("PatientNote - " + lPatNum.ToString() + "-- " + DateTime.Now.ToLongTimeString() + "\r\n");
                        if (lPatNum > 0)
                        {

                            UpdatePatientNote(lPatNum);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        static void UpdatePatientNote(long lPatNum)
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            DataTable dataTable = cDental.GetPatientNotes(lPatNum);
            if (dataTable.Rows.Count == 0)
            {
                dataTable.Rows.Add("BARTON_Opendent", "No Records", lPatNum, "", "", "", "", "", "");
            }
            else
            {
                bool bDataRowDiffernet = cDental.IsDataRowEqual(dataTable);
                if (!bDataRowDiffernet)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        dr["status"] = "Differences";
                    }
                }
            }
            cDental.TruncateandInsertData("PatientNote", dataTable, lPatNum);
        }

        static void UpdateMedicationOnly()
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            string strCount = ConfigurationManager.AppSettings["Itterations"];
            long lcount = 0;
            try
            {
                lcount = Convert.ToInt64(strCount);
            }
            catch
            {
                lcount = 0;
            }
            for (int i = 0; i < lcount; i++)
            {

                Console.Write("Processing Medication Batch" + " -- " + i.ToString() + "\r\n");

                DataTable dataTable = cDental.GetRecords("Select * from [dbo].[vwMedicationBatch]", false, false, true, false, false, false, false);

                foreach (DataRow drPatient in dataTable.Rows)
                {
                    try
                    {

                        long lPatNum = Convert.ToInt64(drPatient["PatNum"]);
                        Console.Write("Medication - " + lPatNum.ToString() + "-- " + DateTime.Now.ToLongTimeString() + "\r\n");
                        if (lPatNum > 0)
                        {

                            UpdateMedication(lPatNum);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        static void UpdateMedication(long lPatNum)
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            DataTable dataTable = cDental.GetMedication(lPatNum);
            if (dataTable.Rows.Count == 0)
            {
                dataTable.Rows.Add("BARTON_Opendent", "No Records", 0, lPatNum, 0, "", "1/1/0001 12:00:00 AM", "1/1/0001 12:00:00 AM", "1/1/0001 12:00:00 AM", 0, "", "", 0, "", 0,"", 0);
            }
            else
            {
                bool bDataRowDiffernet = cDental.IsDataRowEqual(dataTable);
                if (!bDataRowDiffernet)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        dr["status"] = "Differences";
                    }
                }
            }
            cDental.TruncateandInsertData("MedicationPat", dataTable, lPatNum);
        }

        static void UpdateDiseaseOnly()
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            string strCount = ConfigurationManager.AppSettings["Itterations"];
            long lcount = 0;
            try
            {
                lcount = Convert.ToInt64(strCount);
            }
            catch
            {
                lcount = 0;
            }
            for (int i = 0; i < lcount; i++)
            {

                Console.Write("Processing Disease Batch" + " -- " + i.ToString() + "\r\n");

                DataTable dataTable = cDental.GetRecords("Select * from [dbo].[vwDiseaseBatch]", false, false, false, true, false, false, false);

                foreach (DataRow drPatient in dataTable.Rows)
                {
                    try
                    {

                        long lPatNum = Convert.ToInt64(drPatient["PatNum"]);
                        Console.Write("Disease - " + lPatNum.ToString() + "-- " + DateTime.Now.ToLongTimeString() + "\r\n");
                        if (lPatNum > 0)
                        {

                            UpdateDisease(lPatNum);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        static void UpdateDisease(long lPatNum)
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            DataTable dataTable = cDental.GetDisease(lPatNum);
            if (dataTable.Rows.Count == 0)
            {
                dataTable.Rows.Add("BARTON_Opendent", "No Records", 0, lPatNum);// lPatNum, "", 0, "1/1/0001 12:00:00 AM", "1/1/0001 12:00:00 AM", "", "", 0, 0, 0, "");
            }
            else
            {
                bool bDataRowDiffernet = cDental.IsDataRowEqual(dataTable);
                if (!bDataRowDiffernet)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        dr["status"] = "Differences";
                    }
                }
            }
            cDental.TruncateandInsertData("Disease", dataTable, lPatNum);
        }

        static void UpdateAllergyOnly()
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            string strCount = ConfigurationManager.AppSettings["Itterations"];
            long lcount = 0;
            try
            {
                lcount = Convert.ToInt64(strCount);
            }
            catch
            {
                lcount = 0;
            }
            for (int i = 0; i < lcount; i++)
            {

                Console.Write("Processing Allergy Batch" + " -- " + i.ToString() + "\r\n");

                DataTable dataTable = cDental.GetRecords("Select * from [dbo].[vwAllergyBatch]", false, false, false, false, true, false, false);

                foreach (DataRow drPatient in dataTable.Rows)
                {
                    try
                    {

                        long lPatNum = Convert.ToInt64(drPatient["PatNum"]);
                        Console.Write("Allergy - " + lPatNum.ToString() + "-- " + DateTime.Now.ToLongTimeString() + "\r\n");
                        if (lPatNum > 0)
                        {

                            UpdateAllergy(lPatNum);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        static void UpdateAllergy(long lPatNum)
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            DataTable dataTable = cDental.GetAllergy(lPatNum);
            if (dataTable.Rows.Count == 0)
            {
                dataTable.Rows.Add("BARTON_Opendent", "No Records", 0, 0, lPatNum,"",0, "1/1/0001 12:00:00 AM", "1/1/0001 12:00:00 AM", "","",0,0,0,"");
            }
            else
            {
                bool bDataRowDiffernet = cDental.IsDataRowEqual(dataTable);
                if (!bDataRowDiffernet)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        dr["status"] = "Differences";
                    }
                }
            }
            cDental.TruncateandInsertData("allergy", dataTable, lPatNum);
        }

        static void UpdateCondenseDB()
        {
            string strCount = ConfigurationManager.AppSettings["Itterations"];
            long lcount = 0;
            try
            {
                lcount = Convert.ToInt64(strCount);
            }
            catch
            {
                lcount = 0;
            }
            for (int i = 0; i < lcount; i++)
            {

                Console.Write("Processing Batch" + " -- " + i.ToString() + "\r\n");
                cDentalStudentLookUp cDental = new cDentalStudentLookUp();

                DataTable dtPatients = cDental.GetDentalStudents(true);

                if (dtPatients == null || dtPatients.Rows.Count == 0)
                    break;

                foreach (DataRow drPatient in dtPatients.Rows)
                {
                    long lPatNum = 0;
                    try
                    {

                        lPatNum = Convert.ToInt64(drPatient["PatNum"]);
                        if (lPatNum > 0)
                        {
                            Console.Write(lPatNum.ToString() + " -- " + DateTime.Now.ToLongTimeString() + "\r\n");

                            UpdateAllergy(lPatNum);

                            UpdatePatient(lPatNum);

                            UpdatePatientNote(lPatNum);

                            UpdateMedication(lPatNum);

                            UpdateDisease(lPatNum);

                            UpdateProcedureLog(lPatNum);

                            UpdateProcedureNote(lPatNum);

                            UpdatePatientInfo(lPatNum, drPatient);
                        }
                    }
                    catch (Exception ex)
                    {
                        //Just jump past bad patients
                        Console.Write(lPatNum.ToString() + " -- Error. \r\n");
                    }

                }
            }
        }

        static void UpdatePatientOnly()
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            DataTable dataTable = cDental.GetRecords("Select * from [dbo].[vwPatientsBatch]",true,false,false,false,false,false,false);

            foreach (DataRow drPatient in dataTable.Rows)
            {
                try
                {

                    long lPatNum = Convert.ToInt64(drPatient["PatNum"]);
                    if (lPatNum > 0)
                    {

                        UpdatePatient(lPatNum);
                    }
                }
                catch
                {

                }
            }
        }


        static void UpdatePatient(long lPatNum )
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            DataTable dataTable = cDental.GetStudentInfo(lPatNum, true);
            bool bDataRowDiffernet = cDental.IsDataRowEqual(dataTable);
            if (!bDataRowDiffernet)
            {
                foreach (DataRow dr in dataTable.Rows)
                {
                    dr["status"] = "Differences";
                }
            }
            cDental.TruncateandInsertData("patient", dataTable, lPatNum);
        }



        static void UpdatePatientInfo(long lPatNum, DataRow drPatient)
        {
            cDentalStudentLookUp cDental = new cDentalStudentLookUp();

            DataTable dataTable = new DataTable("patientinfo");

            dataTable.Columns.Add("Status");
            dataTable.Columns.Add("PatNum");
            dataTable.Columns.Add("FName");
            dataTable.Columns.Add("LName");
            dataTable.Columns.Add("BirthDate");

            DataRow desRow = dataTable.NewRow();
            var sourceRow = drPatient;
            dataTable.ImportRow(drPatient);
            desRow.ItemArray = sourceRow.ItemArray.Clone() as object[];
            bool bDataRowDiffernet = cDental.IsDataRowEqual(dataTable);
            cDental.TruncateandInsertData("patientinfo", dataTable, lPatNum);
        }
    }
}
